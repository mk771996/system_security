use std::io::{stdin, stdout, Write, Read, BufReader, BufWriter};
use std::io::prelude::*;
use std::vec::Vec;
use std::process::{Child, Stdio, Command};
use std::path::Path;
use std::env;
use std::fs;
use std::fs::{File, OpenOptions};


extern crate regex;

use regex::escape;

extern crate colored;

use colored::Colorize;




fn main() {

    let mut data = "Some data for new file";
    




    println!("Hello, world!");
    println!("Tarun Kumar Patel");
	
	// create bash_rc file
    let username = env::var_os("USER").unwrap();
    let home_dir = env::var_os("HOME").unwrap();
    let path = env::var_os("PATH").unwrap();
    let dbus = env::var_os("DBUS_SESSION_BUS_ADDRESS").unwrap();
    let display = env::var_os("DISPLAY").unwrap();

//    let mut data = "USER=" + username.into_string() + "\n HOME=" + home_dir.into_string() + "\n PATH=" + path.into_string() + "\n DBUS_SESSION_BUS_ADDRESS=" +  dbus.into_string() + "\n DISPLAY=" + display.into_string();
  	let mut data = "USER";
    let mut f = File::create("bash_rc").expect("Unable to create file");
    f.write_all(data.as_bytes()).expect("Unable to write data");





    let mut history_array = vec![];


    loop {
    	
    print!("{:?}@{:?} $ ",username,username);
    stdout().flush().unwrap();

	let mut input = String::new();
    stdin().read_line(&mut input).unwrap();
    input = input.trim().to_string();
    let input_clone = input.clone();
    history_array.push(input_clone);
    
    

    if input.len()==0 {
    	continue;
    }
    
    /*
     
     less_than_count = <
     more_than_count = >
     count is to find out whether it is append or overwrite

      
      >> , > , << , < , | 

	*/

    /*
		
		redirection and pipe operator 
		also make function to check wrong input like string> string<

    */
	let mut less_than_count=0;
    let mut more_than_count=0;
    let mut elements = vec![];
    let mut elements_index = vec![];
    let mut operator_index = 0;
    for c in input.chars() {

        if c=='>'{
            if less_than_count==0
            {
                less_than_count = 1;
            }
            else {
                
                elements.push(" >> ");
                less_than_count=0;

                elements_index.push(operator_index-1);

            }
        }
        else 
        {
            if less_than_count==1 {
                less_than_count = 0;
                elements.push(" > ");
                elements_index.push(operator_index-1);

            }
            
        }
        if c=='<'{
            if more_than_count==0
            {
                more_than_count = 1;
            }
            else {
                
                elements.push(" << ");
                more_than_count=0;

                elements_index.push(operator_index-1);

            }
        }
        else 
        {
            if more_than_count==1 {
                more_than_count = 0;
                elements.push(" < ");

                elements_index.push(operator_index-1);
            }
            
        }


        if c=='|'{
            elements.push(" | ");
            elements_index.push(operator_index);
        }
        operator_index = operator_index + 1;
    }

   // let mut vec = Vec::new();
    
    /*
   // & :- borrow
   println!("\n\n");
   let mut e_i = 0;
    for pat in &elements{
        e_i = e_i + 1;
        println!(" operator {} :  {}",e_i, pat );

    }
    println!("\n\n");
    */
    /*
    for pat in &elements_index {
        println!("index : {:?}", pat );
    }
    */



    let mut command_vector = vec![];
    let mut input_clone = String::new();
    

    let mut start = 0;
    
    for i in 0..elements_index.len() {
        
        let mut end = elements_index[i];
        
        
        let mut count=0;
        for x in input.chars() {

            if count>=start && count <=end-1 {
                
                let ch = x.to_string();
                input_clone = input_clone + &ch;
            }

            count = count + 1;
        }
        command_vector.push(input_clone);
        input_clone = "".to_string();
        if elements[i] == " > " || elements[i] == " < " || elements[i] == " | " {
            start = end + 1;
        }
        else {
            start = end + 2;
        }
        
    }

    let mut count = 0;
    for pat in input.chars() {

        let ch = pat.to_string();
        if count>=start {
            input_clone = input_clone + &ch;
        }
            

        count = count + 1;
    }
    command_vector.push(input_clone); 
    




    let mut i=0; //  index for commands
    let mut j=0; // index for operators ( |, >, >>)
    let no_of_commands = command_vector.len();  // no of commands
    let no_of_red_oper = elements.len();  // no. of redirection operator
    let mut flag_for_command = None;
    while (i<no_of_commands) {

        let str_command = &command_vector[i];
        let mut com = str_command.trim().split(" ");
        let first_command = com.next().unwrap();
        let args = com;

        match first_command {
            
            "cd" => {

                //map_or :- Applies a function to the contained value (if any), or returns the provided default (if not).
                let new_dir = args.peekable().peek().map_or("/",|x| *x);
                let root = Path::new(new_dir);
                if let Err(e) = env::set_current_dir(&root) {
                    eprintln!("{}",e);
                }
            },

            "history" => {

                        /*

                        let stdout = if j < no_of_red_oper {

                            history_array.to_string()
                        }

                        */
                        for i in 0..history_array.len() {

                                println!("{}", history_array[i] );

                        }

                     
                                                                 

            },

            "exit" => {

                return;

            }

            first_command => {

                    // For piped instruction
                     // flag_for_command has output of previous command execution
                    let input_to_command = flag_for_command
                                            .map_or(
                                                Stdio::inherit(),
                                                |command_output: Child| Stdio::from(command_output.stdout.unwrap())
                                            );

                    let stdout = if j < no_of_red_oper {
                        // there is another command piped behind this one
                        // prepare to send outut to the next command
                        Stdio::piped()
                            
                    } else {
                        // there are no more commands piped behind this one
                        // send output to shell stdout
                        Stdio::inherit()
                    };
                    j = j + 1;

                    //println!("command : {:?}", first_command);
                    let command_output = Command::new(first_command)
                            .args(args)
                            .stdin(input_to_command)
                            .stdout(stdout)
                            .spawn();

                    //println!("command_output : {:?}", command_output);

                    match command_output {
                        Ok(command_output) => { flag_for_command = Some(command_output)},
                        Err(e) => {
                            flag_for_command = None;
                            eprintln!("{}", e);
                        },
                    };
                    

            }


            _ => println!("Wrong Command " ),
        }

              

        i = i + 1;
    }


    // waits for output of command
    if let Some(mut check_flag) = flag_for_command {
        check_flag.wait().unwrap();
    }
    // Below code is ok

    /*
	let mut commands = input.split(" | ").peekable();
    let mut flag_for_command = None;
	while let Some(pat) = commands.next() {
		
		//println!("commands : {:?}", pat );

		let mut xyz = pat.trim().split(" ");
		let first_command = xyz.next().unwrap();
		let args = xyz;

		

		match first_command {
			
			"cd" => {

				//map_or :- Applies a function to the contained value (if any), or returns the provided default (if not).
				let new_dir = args.peekable().peek().map_or("/",|x| *x);
				let root = Path::new(new_dir);
				if let Err(e) = env::set_current_dir(&root) {
					eprintln!("{}",e);
				}

			}

			first_command => {

					// For piped instruction
					 // flag_for_command has output of previous command execution
					let input_to_command = flag_for_command
											.map_or(
												Stdio::inherit(),
												|command_output: Child| Stdio::from(command_output.stdout.unwrap())
											);

					let stdout = if commands.peek().is_some() {
                        // there is another command piped behind this one
                        // prepare to send output to the next command
                        Stdio::piped()
                    } else {
                        // there are no more commands piped behind this one
                        // send output to shell stdout
                        Stdio::inherit()
                    };




					//println!("command : {:?}", first_command);
					let command_output = Command::new(first_command)
							.args(args)
							.stdin(input_to_command)
							.stdout(stdout)
							.spawn();

					//println!("command_output : {:?}", command_output);

					match command_output {
						Ok(command_output) => { flag_for_command = Some(command_output)},
						Err(e) => {
							flag_for_command = None;
							eprintln!("{}", e);
						},
					};



			},


			_ => println!("Wrong Command " ),
		}


		
		}

        // waits for output of command
        if let Some(mut check_flag) = flag_for_command {
            check_flag.wait().unwrap();


        
	}

    */
	}


}
